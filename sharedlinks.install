<?php

/**
 * @file
 * Install functions for the Shared Links module
 *
 */

/**
 * Implementation of hook_install().
 */
function sharedlinks_install() {
  drupal_install_schema('sharedlinks');
}

/**
 * Implementation of hook_uninstall().
 */
function sharedlinks_uninstall() {
  drupal_uninstall_schema('sharedlinks');
  variable_delete('sharedlinks_local_links');
  variable_delete('sharedlinks_max_links');
}

/**
 * Implementation of hook_schema().
 */
function sharedlinks_schema() {
  $schema['sharedlinks_sites'] = array(
    'description' => t("Stores sites from which to pull keyword links."),
    'fields' => array(
      'sid' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t("The site's site id."),
      ),
      'root' => array(
        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE,
        'description' => t("The root of the Drupal site. Include 'http://'."),
      ),
    ),
    'primary key' => array('sid'),
  );

  $schema['sharedlinks_links'] = array(
    'description' => t("Stores sites from which to pull keyword links."),
    'fields' => array(
      'lid' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t("The link id.  This is the primary key."),
      ),
      'sid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0, // A value of '0' means the local site
        'description' => t("The site id from {sharedlinks_sites}.sid."),
      ),
      'keywords' => array(
        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE,
        'description' => t("The keyword or keyphrase that will become the link."),
      ),
      'path' => array(
        'type' => 'varchar',
        'length' => 128,
        'description' => t("The path to which the keywords will link."),
      ),
    ),
    'primary key' => array('lid'),
    'indexes' => array(
      'sharedlinks_sid' => array('sid'),
    ),
  );
  return $schema;
}