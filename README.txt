
-- SUMMARY --

This module allows a site administrator to list keywords and/or keyphrases along
with a path to which each should link.  The site administrator can also add sites
from which additional keywords/keyphrases and paths can be collected.

This is particularly useful when a site administrator manages multiple sites and
wishes to share links among them for SEO purposes.

For a full description of the module, visit the project page:
  http://drupal.org/project/sharedlinks

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/sharedlinks


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* Configure user permissions in Administer >> User management >> Permissions >>
  sharedlinks module:

  - administer shared links

    Users in roles with the "administer shared links" permission will be
    able to add local links to share, and add sites from which to collect
    additional links.

* Add links and sites in Administer >> Site configuration >> Shared Links.
