<?php

/**
 * @file
 * sharedlinks.tpl.php
 * Default theme implementation to display the shared links in a block.
 *
 * Available variables:
 *
 * - $links: an array of links formatted by l().
 *
  *
 */

?>
<div id="sharedlinks">
<?php print implode(' ', $links); ?>
</div>