<?php


/**
 * @file
 * Settings forms for the Shared Links module
 *
 */

/**
 * Lists current links for local site and provides a method to customize.
 */
function sharedlinks_admin_links() {
  $header = array(
    array('data' => t('Keywords'), 'field' => 'keywords'),
    array('data' => t('Path'), 'field' => 'path'),
    array('data' => t('Operations'), 'colspan' => 2),
  );
  $sql = "SELECT lid, keywords, path
    FROM {sharedlinks_links}
    WHERE sid = 0";
  $result = pager_query($sql . tablesort_sql($header), 10);
  $rows = array();
  while ($row = db_fetch_object($result)) {
    $path = $row->path ? $row->path : '<front>';
    $rows[] = array(
      $row->keywords,
      l($path, $path),
      l(t('Edit'), 'admin/settings/sharedlinks/link/edit/'. $row->lid),
      l(t('Delete'), 'admin/settings/sharedlinks/link/delete/'. $row->lid),
    );
  }
  if (!$rows) {
    $rows[] =array(
      array(
      'data' => t('Use ') . l(t('this form'), 'admin/settings/sharedlinks/link/add') . t(' to add links.'),
      'colspan' => 4,
      )
    );
  }
  $output = theme('table', $header, $rows);
  $output .= theme('pager', NULL, 10);
  return $output;
}


/**
 * Menu callback; handles pages for creating and editing links.
 */
function sharedlinks_admin_link_edit($lid = NULL) {
  if ($lid) {
    $link = sharedlinks_link_load($lid);
    drupal_set_title(check_plain($link['root']));
    $output = drupal_get_form('sharedlinks_admin_link_form', $link);
  }
  else {
    $output = drupal_get_form('sharedlinks_admin_link_form');
  }

  return $output;
}

/**
 * Return a form for editing or creating an individual link to share.
 *
 * @ingroup forms
 * @see sharedlinks_admin_link_form_validate()
 * @see sharedlinks_admin_link_form_submit()
 */
function sharedlinks_admin_link_form(&$form_state, $edit = array('keywords' => '', 'path' => '', 'lid' => NULL)) {

  $form['#alias'] = $edit;

  $form['keywords'] = array(
    '#type' => 'textfield',
    '#title' => t('Keyword or keyphrase'),
    '#default_value' => $edit['keywords'],
    '#maxlength' => 128,
    '#size' => 45,
    '#description' => t('Specify the keyword or keyphrase you want linked to your site.'),
    '#required' => TRUE,
  );
  $form['path'] = array(
    '#type' => 'textfield',
    '#title' => t('Internal Path'),
    '#default_value' => $edit['path'],
    '#maxlength' => 64,
    '#size' => 45,
    '#description' => t('Specify the internal path to which these keywords will link. For example: node/28, forum/1, taxonomy/term/1+2. Leave blank for &lt;front&gt;.'),
    '#field_prefix' => url(NULL, array('absolute' => TRUE)) . (variable_get('clean_url', 0) ? '' : '?q='),
  );
  if ($edit['lid']) {
    $form['lid'] = array('#type' => 'hidden', '#value' => $edit['lid']);
    $form['submit'] = array('#type' => 'submit', '#value' => t('Update link'));
  }
  else {
    $form['submit'] = array('#type' => 'submit', '#value' => t('Create new link'));
  }

  return $form;
}



/**
 * Verify that a new link is valid
 */
function sharedlinks_admin_link_form_validate($form, &$form_state) {
  $path = $form_state['values']['path'] ? $form_state['values']['path'] : '/';
  $item = menu_get_item($path);
  if ($item && !$item['access']) {
    form_set_error('src', t("The path '@link_path' is either invalid or you do not have access to it.", array('@link_path' => $path)));
  }
}

/**
 * Save a new link to the database.
 */
function sharedlinks_admin_link_form_submit($form, &$form_state) {
  $keywords = $form_state['values']['keywords'];
  $path = $form_state['values']['path'];
  $lid = $form_state['values']['lid'];
  //print $keywords . $path . $lid;
  if ($lid) {
    db_query("UPDATE {sharedlinks_links} SET keywords = '%s', path = '%s' WHERE lid = %d", $keywords, $path, $lid);
  }
  else {
    db_query("INSERT INTO {sharedlinks_links} (keywords, path) VALUES ('%s', '%s')", $keywords, $path);
  }

  drupal_set_message(t('The link has been saved.'));
  $form_state['redirect'] = 'admin/settings/sharedlinks';
  return;
}

/**
 * Menu callback; confirms deleting a link
 */
function sharedlinks_admin_link_delete_confirm($form_state, $lid) {
  $link = sharedlinks_link_load($lid);
  if (user_access('administer shared links')) {
    $form['lid'] = array('#type' => 'value', '#value' => $lid);
    $output = confirm_form($form,
      t('Are you sure you want to delete link %keywords?', array('%keywords' => $link['keywords'])),
      isset($_GET['destination']) ? $_GET['destination'] : 'admin/settings/sharedlinks/link');
  }
  return $output;
}

/**
 * Execute link deletion
 */
function sharedlinks_admin_link_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    sharedlinks_admin_link_delete($form_state['values']['lid']);
    $form_state['redirect'] = 'admin/settings/sharedlinks/link';
    return;
  }
}

/**
 * Post-confirmation; delete a link.
 */
function sharedlinks_admin_link_delete($lid = 0) {
  db_query('DELETE FROM {sharedlinks_links} WHERE lid = %d', $lid);
  drupal_set_message(t('The link has been deleted.'));
}

/**
 * Lists current sites from which to pull external links and
 * provides a method to customize.
 */
function sharedlinks_admin_sites() {
  $sql = "SELECT sid, root
    FROM {sharedlinks_sites}
    WHERE sid != 0";
  $result = db_query($sql);
  $rows = array();
  while ($row = db_fetch_object($result)) {
    $rows[] = array(
      $row->root,
      l(t('Edit'), 'admin/settings/sharedlinks/site/edit/'. $row->sid),
      l(t('Delete'), 'admin/settings/sharedlinks/site/delete/'. $row->sid),
    );
  }
  $header = array(
    array('data' => t('Site Root'), 'field' => 'root'),
    array('data' => t('Operations'), 'colspan' => 2),
  );
  if (!$rows) {
    $rows[] = array(
      array(
      'data' => t('Use ') . l(t('this form'), 'admin/settings/sharedlinks/site/add') . t(' to add sites.'),
      'colspan' => 3,
      ),
    );
  }
  return theme('table', $header, $rows);
}

/**
 * Menu callback; handles pages for creating and editing linked sites.
 */
function sharedlinks_admin_site_edit($sid = NULL) {
  if ($sid) {
    $site = sharedlinks_site_load($sid);
    drupal_set_title(check_plain($site['root']));
    $output = drupal_get_form('sharedlinks_admin_site_form', $site);
  }
  else {
    $output = drupal_get_form('sharedlinks_admin_site_form');
  }

  return $output;
}

/**
 * Return a form for editing or creating an individual site from which to pull links.
 *
 * @ingroup forms
 * @see sharedlinks_admin_site_form_validate()
 * @see sharedlinks_admin_site_form_submit()
 */
function sharedlinks_admin_site_form(&$form_state, $edit = array('root' => '', 'sid' => NULL)) {

  $form['#alias'] = $edit;

  $form['root'] = array(
    '#type' => 'textfield',
    '#title' => t('Site from which to pull links'),
    '#default_value' => $edit['root'],
    '#maxlength' => 128,
    '#size' => 60,
    '#description' => t('Specify the root of the site from which to pull links.  Example: http://example.com/drupal'),
    '#required' => TRUE,
  );
  if ($edit['sid']) {
    $form['sid'] = array('#type' => 'hidden', '#value' => $edit['sid']);
    $form['submit'] = array('#type' => 'submit', '#value' => t('Update site'));
  }
  else {
    $form['submit'] = array('#type' => 'submit', '#value' => t('Create new site'));
  }

  return $form;
}



/**
 * Verify that a new link is valid
 */
function sharedlinks_admin_site_form_validate($form, &$form_state) {
  $site = $form_state['values']['root'];
  if (!ereg("^http", $site)) {
    form_set_error('src', t("The site '@site' is not valid.  Be sure to start with http://.", array('@site' => $site)));
  }
}

/**
 * Save a new link to the database.
 */
function sharedlinks_admin_site_form_submit($form, &$form_state) {
  $site = $form_state['values']['root'];
  $sid = $form_state['values']['sid'];
  //print $keywords . $path . $lid;
  if ($sid) {
    db_query("UPDATE {sharedlinks_sites} SET root = '%s' WHERE sid = %d", $site, $sid);
  }
  else {
    db_query("INSERT INTO {sharedlinks_sites} (root) VALUES ('%s')", $site);
    $sid = db_result(db_query('SELECT MAX(sid) FROM {sharedlinks_sites}'));
  }
  drupal_set_message(t('The site has been saved.'));
  if (sharedlinks_sitelinks_load($sid)) {
    drupal_set_message(t('Links have been collected for %site.', array('%site' => $site)));
  }
  else {
    drupal_set_message(t('No shared links have been found for %site.', array('%site' => $site)), 'warning');
  }
  $form_state['redirect'] = 'admin/settings/sharedlinks/site';
  return;
}

/**
 * Menu callback; confirms deleting a site
 */
function sharedlinks_admin_site_delete_confirm($form_state, $sid) {
  $site = sharedlinks_site_load($sid);
  if (user_access('administer shared links')) {
    $form['sid'] = array('#type' => 'value', '#value' => $sid);
    $output = confirm_form($form,
      t('Are you sure you want to delete site %site?', array('%site' => $site['root'])),
      isset($_GET['destination']) ? $_GET['destination'] : 'admin/settings/sharedlinks/site');
  }
  return $output;
}

/**
 * Execute site deletion
 */
function sharedlinks_admin_site_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    sharedlinks_admin_site_delete($form_state['values']['sid']);
    $form_state['redirect'] = 'admin/settings/sharedlinks/site';
    return;
  }
}

/**
 * Post-confirmation; delete a site.
 */
function sharedlinks_admin_site_delete($sid = 0) {
  db_query('DELETE FROM {sharedlinks_sites} WHERE sid = %d', $sid);
  db_query('DELETE FROM {sharedlinks_links} WHERE sid = %d', $sid);
  drupal_set_message(t('The site has been deleted.'));
}

/**
 * Form builder. Configure sharedlinks.
 *
 * @see system_settings_form()
 */
function sharedlinks_admin_settings() {
  $form['sharedlinks_local_links'] = array(
    '#type' => 'checkbox',
    '#title' => t('Include local links in this site\'s Shared Links block.'),
    '#default_value' => variable_get('sharedlinks_local_links', 1),
  );
  $form['sharedlinks_max_links'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of links to display'),
    '#default_value' => variable_get('sharedlinks_max_links', 0),
    '#description' => t('Enter the maximum number of links to display in
      this site\'s Shared Links block (0 for no limit).'),
    '#size' => 3,
  );
  return system_settings_form($form);
}

/**
 * Validate the sharedlinks configuration form.
 */
function sharedlinks_admin_settings_validate($form, $form_state) {
  $limit = $form_state['values']['sharedlinks_max_links'];
  if (!is_numeric($limit) || $limit < 0) {
    form_set_error('sharedlinks_count_links', t('Please enter a positive number, or 0 for no limit.'));
  }
}
